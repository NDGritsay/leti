#include <cstdlib>
#include "ListSet.h"

ListSet::ListSet() {
    head = nullptr;
    power = 0;
}

bool ListSet::contains(char elem) const {
    ListElemSet *tmp = head;
    do {
        if (tmp->c == elem)
            return true;
    } while ((tmp = tmp->pNext) != nullptr);

    return false;
}

void ListSet::fillSet(int subSetCardinality) {
    ListElemSet *tmp = head = new ListElemSet;
    power = subSetCardinality;


    for (char i = 0; i < N; i++) {
        tmp->c = i + '0';
        if (i + 1 != N)
            tmp = tmp->pNext = new ListElemSet;
    }

    for (int i = 0; i < subSetCardinality; i++) {
        int index = rand() % (N - i);
        swapElem(i, i + index);
    }

    tmp = head;
    for (int i = 0; i < subSetCardinality - 1; i++)
        tmp = tmp->pNext;
    if (subSetCardinality == 0) {
        deleteListElemSet(head);
        head = nullptr;
    } else {
        deleteListElemSet(tmp->pNext);
        tmp->pNext = nullptr;
    }
}

void ListSet::swapElem(int i1, int i2) {
    char cpy;
    ListElemSet *tmp = head, *elem1, *elem2;

    for (int i = 0; i < i1; i++)
        tmp = tmp->pNext;
    elem1 = tmp;

    tmp = head;
    for (int i = 0; i < i2; i++)
        tmp = tmp->pNext;
    elem2 = tmp;

    cpy = elem1->c;
    elem1->c = elem2->c;
    elem2->c = cpy;
}

void ListSet::addElem(const char c) {
    ListElemSet *tmp = head;
    if (head) {
        while (tmp->pNext)
            tmp = tmp->pNext;
        tmp->pNext = new ListElemSet;
        tmp->pNext->c = c;
    } else {
        head = new ListElemSet;
        head->c = c;
    }
}

ListSet::ListSet(int power) {
    fillSet(power);
    this->power = power;
}

ListSet::~ListSet() {
    deleteListElemSet(head);
    head = nullptr;
}

std::ostream &operator<<(std::ostream &os, const ListSet &set) {
    os << "listSet: ";
    if (set.power == 0)
        os << " empty";
    ListElemSet *tmp = set.head;
    while (tmp) {
        os << tmp->c << " ";
        tmp = tmp->pNext;
    }

    os << std::endl;
    return os;
}

ListSet::ListSet(const ListSet &B) {
    power = B.power;
    ListElemSet *tmpB = B.head;
    ListElemSet *tmpA;

    if (tmpB) {
        head = tmpA = new ListElemSet;
        tmpA->c = tmpB->c;
    } else {
        head = nullptr;
        return;
    }

    while ((tmpB = tmpB->pNext)) {
        tmpA = tmpA->pNext = new ListElemSet;
        tmpA->c = tmpB->c;
    }
}

ListSet::ListSet(ListSet &&B) {
    power = B.power;
    head = B.head;
    B.head = nullptr;
}

ListSet &ListSet::operator=(const ListSet &B) {
    if (this->head == B.head && this->power == B.power)
        return *this;

    deleteListElemSet(head);
    power = B.power;
    ListElemSet *tmpB = B.head;
    ListElemSet *tmpA;

    if (tmpB) {
        head = tmpA = new ListElemSet;
        tmpA->c = tmpB->c;
    } else {
        head = nullptr;
        return *this;
    }

    while ((tmpB = tmpB->pNext)) {
        tmpA = tmpA->pNext = new ListElemSet;
        tmpA->c = tmpB->c;
    }
    return *this;
}

ListSet &ListSet::operator=(ListSet &&B) {
    power = B.power;
    head = B.head;
    B.head = nullptr;
    return *this;
}

ListSet &ListSet::operator|(const ListSet &B) const {
    ListSet *resSet = new ListSet(*this);
    ListElemSet *tmp = B.head;
    while (tmp) {
        if (!resSet->contains(tmp->c)) {
            resSet->addElem(tmp->c);
            (resSet->power)++;
        }
        tmp = tmp->pNext;
    }

    return *resSet;
}

ListSet &ListSet::operator&(const ListSet &B) const {
    ListSet *resSet = new ListSet;
    ListElemSet *tmp = head;

    while (tmp) {
        if (B.contains(tmp->c)) {
            resSet->addElem(tmp->c);
            (resSet->power)++;
        }
        tmp = tmp->pNext;
    }

    return *resSet;
}

void ListSet::deleteListElemSet(ListElemSet *set) {
    ListElemSet *tmp;

    while (set) {
        tmp = set;
        set = set->pNext;
        delete (tmp);
    }
}


