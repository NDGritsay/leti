#include "Set.h"
#include <string.h>


Set::Set() {
    power = 0;
    set = new char[N + 1];
    *set = EOS;
}

Set::Set(int power) : power(power) {
    fillSet(power);
    this->power = power;
}

Set::~Set() {
    delete[] set;
    set = nullptr;
}


Set &Set::operator|(const Set &B) const {
    Set *resSet = new Set(*this);
    for (int i = 0; B.set[i] != EOS; i++)
        if (!resSet->contains(B.set[i])) {
            resSet->set[resSet->power] = B.set[i];
            (resSet->power)++;
            resSet->set[resSet->power] = EOS;
        }

    return *resSet;
}

Set &Set::operator&(const Set &B) const {
    Set *resSet = new Set;
    for (int i = 0; this->set[i] != EOS; i++)
        if (B.contains(this->set[i])) {
            resSet->set[resSet->power] = this->set[i];
            (resSet->power)++;
            resSet->set[resSet->power] = EOS;
        }

    return *resSet;
}


void Set::fillSet(int subSetCardinality) {
    set = new char[N + 1];

    for (char i = 0; i < N; i++)
        set[i] = i + '0';

    for (int i = 0; i < subSetCardinality; i++) {
        int index = rand() % (N - i);

        char c = set[i];
        set[i] = set[i + index];
        set[i + index] = c;
    }

    set[subSetCardinality] = EOS;
}

bool Set::contains(char c) const {
    for (char i = 0; this->set[i] != EOS; i++)
        if (this->set[i] == c)
            return true;
    return false;
}

std::ostream &operator<<(std::ostream &os, const Set &set) {
    os << "set: ";
    if (set.power == 0)
        os << " empty";
    for (int i = 0; i < set.power; ++i)
        os << set.set[i] << " ";

    os << std::endl;
    return os;
}

Set::Set(const Set &B) {
    this->power = B.power;
    set = new char[N + 1];
    memcpy(this->set, B.set, (N + 1) * sizeof(char));
}

Set::Set(Set &&B) {
    this->power = B.power;
    set = B.set;
    B.set = nullptr;
}

Set &Set::operator=(const Set &B) {
    if (this->set == B.set && this->power == B.power)
        return *this;
    this->power = B.power;
    set = new char[N + 1];
    memcpy(this->set, B.set, (N + 1) * sizeof(char));
    return *this;
}

Set &Set::operator=(Set &&B) {
    if (this->set == B.set && this->power == B.power)
        return *this;
    this->power = B.power;
    set = B.set;
    B.set = nullptr;
    return *this;
}
