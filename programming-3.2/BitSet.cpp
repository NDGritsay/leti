#include "BitSet.h"
#include <string.h>

std::ostream &operator<<(std::ostream &os, const BitSet &set) {
    os << "bitSet: ";
    if (set.power == 0)
        os << " empty";
    for (int i = 0; i < set.N; i++)
        if (set.set[i])
            os << i << " ";

    os << std::endl;
    return os;
}

bool BitSet::contains(int i) const {
    return set[i];
}

void BitSet::fillSet(int subSetCardinality) {
    char *tmp = new char[N + 1];
    for (char i = 0; i < N; i++)
        tmp[i] = i + '0';
    for (int i = 0; i < subSetCardinality; i++) {
        int index = rand() % (N - i);

        char c = tmp[i];
        tmp[i] = tmp[i + index];
        tmp[i + index] = c;
    }

    set = new bool[N];
    for (int i = 0; i < N; i++)
        set[i] = false;
    for (int i = 0; i < subSetCardinality; i++)
        setBit(tmp[i] - '0');

    delete[] tmp;
}

void BitSet::setBit(int i) {
    set[i] = true;
}

BitSet::BitSet() {
    set = new bool[N];
    for (int i = 0; i < N; i++)
        set[i] = false;
    power = 0;
}

BitSet::BitSet(int power) {
    this->power = power;
    fillSet(power);
}

BitSet::BitSet(const BitSet &B) {
    this->power = B.power;
    set = new bool[N];
    memcpy(this->set, B.set, (N) * sizeof(bool));
}

BitSet::BitSet(BitSet &&B) {
    this->power = B.power;
    set = B.set;
    B.set = nullptr;
}

BitSet::~BitSet() {
    delete[] set;
    set = nullptr;
}

BitSet &BitSet::operator|(const BitSet &B) const {
    BitSet *resSet = new BitSet(*this);
    for (int i = 0; i < N; i++)
        if (resSet->set[i] && !resSet->contains(i)) {
            resSet->setBit(i);
            (resSet->power)++;
        }

    return *resSet;
}

BitSet &BitSet::operator&(const BitSet &B) const {
    BitSet *resSet = new BitSet;
    for (int i = 0; i < N; i++)
        if (B.set[i] && B.contains(i)) {
            resSet->setBit(i);
            (resSet->power)++;
        }

    return *resSet;
}

BitSet &BitSet::operator=(const BitSet &B) {
    if (this->set == B.set && this->power == B.power)
        return *this;
    this->power = B.power;
    set = new bool[N];
    memcpy(this->set, B.set, (N) * sizeof(bool));
    return *this;
}

BitSet &BitSet::operator=(BitSet &&B) {
    if (this->set == B.set && this->power == B.power)
        return *this;
    this->power = B.power;
    set = B.set;
    B.set = nullptr;
    return *this;
}
