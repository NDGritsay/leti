cmake_minimum_required(VERSION 3.6)
project(Programming_3_2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Set.cpp Set.h ListSet.cpp ListSet.h BitSet.cpp BitSet.h ComputerWordSet.cpp ComputerWordSet.h)
add_executable(Programming_3_2 ${SOURCE_FILES})