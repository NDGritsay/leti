#ifndef PROGRAMMING_3_2_COMPUTERWORDSET_H
#define PROGRAMMING_3_2_COMPUTERWORDSET_H


#include <iosfwd>
#include <ostream>

class ComputerWordSet {
private:
    int power;
    static const int N = 10;
    int set;

    void fillSet(int n);

    void setBit(int i);

    int countBits(int i) const;

public:
    ComputerWordSet();

    ComputerWordSet(int power);

    ComputerWordSet(const ComputerWordSet &);

    friend std::ostream &operator<<(std::ostream &os, const ComputerWordSet &set);

    ComputerWordSet &operator|(const ComputerWordSet &) const;

    ComputerWordSet &operator&(const ComputerWordSet &) const;

    ComputerWordSet &operator=(const ComputerWordSet &);
};


#endif //PROGRAMMING_3_2_COMPUTERWORDSET_H
