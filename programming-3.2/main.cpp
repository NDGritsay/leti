#include <iostream>
#include <Windows.h>
#include <ctime>
#include <chrono>
#include "Set.h"
#include "ListSet.h"
#include "BitSet.h"
#include "ComputerWordSet.h"

using namespace std;


const int CARDIN = 10;
const int OPERATION_CT = 100000;


//Developed by Nikita Gritsay & Evgeny Dolenko, Nanolution comp(c)
//Compiled with MinGW
int main()
{
    system("title = Algorithms LETI 3.2");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(0));

    long long arraySetTime[10], listSetTime[10], bitArrayTime[10], computerWordTime[10];

    cout << "Welcome to algorithm complexity counter!\n";
    system("pause");

    cout << "\nprocess has started\n";


    for (int i = 0; i <= CARDIN; i++)
    {
        std::chrono::high_resolution_clock::time_point startTime, finishTime;
        Set A(i), B(i), C(i), D(i), res(i);


        startTime = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < OPERATION_CT; j++)
            res = A | B | (C & D);

        cout << "A: " << A;
        cout << "B: " << B;
        cout << "C: " << C;
        cout << "D: " << D;
        cout << "A|B|(C&D)= " << res << endl;

        finishTime = std::chrono::high_resolution_clock::now();
        arraySetTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
    }


    for (int i = 0; i <= CARDIN; i++)
    {
        std::chrono::high_resolution_clock::time_point startTime, finishTime;
        ListSet A(i), B(i), C(i), D(i), res(i);

        startTime = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < OPERATION_CT; j++)
            res = A | B | (C & D);

        cout << "A: " << A;
        cout << "B: " << B;
        cout << "C: " << C;
        cout << "D: " << D;
        cout << "A|B|(C&D)= " << res << endl;

        finishTime = std::chrono::high_resolution_clock::now();
        listSetTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
    }


    for (int i = 0; i <= CARDIN; i++)
    {
        std::chrono::high_resolution_clock::time_point startTime, finishTime;
        BitSet A(i), B(i), C(i), D(i), res(i);

        startTime = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < OPERATION_CT; j++)
            res = A | B | (C & D);

        cout << "A: " << A;
        cout << "B: " << B;
        cout << "C: " << C;
        cout << "D: " << D;
        cout << "A|B|(C&D)= " << res << endl;

        finishTime = std::chrono::high_resolution_clock::now();
        bitArrayTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
    }

    for (int i = 0; i <= CARDIN; i++)
    {
        std::chrono::high_resolution_clock::time_point startTime, finishTime;
        ComputerWordSet A(i), B(i), C(i), D(i), res(i);

        startTime = std::chrono::high_resolution_clock::now();
        for (int j = 0; j < OPERATION_CT; j++)
            res = A | B | (C & D);

        cout << "A: " << A;
        cout << "B: " << B;
        cout << "C: " << C;
        cout << "D: " << D;
        cout << "A|B|(C&D)= " << res << endl;

        finishTime = std::chrono::high_resolution_clock::now();
     computerWordTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime - startTime).count();
    }

    cout << "\nprocessing finished correctly!";

    cout << "\narray time: ";
    for (int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(arraySetTime[i]) << " ";

    cout << ".\nlist time: ";
    for (int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(listSetTime[i]) << " ";

    cout << ".\narray of bits time: ";
    for (int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(bitArrayTime[i]) << " ";

    cout << ".\ncomputer words time: ";
    for (int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(computerWordTime[i]) << " ";
    cout << ".\n";
    system("pause");
}