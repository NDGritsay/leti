#include <cstdlib>
#include "ComputerWordSet.h"

ComputerWordSet::ComputerWordSet() {
    power = 0;
    set = 0;
}

void ComputerWordSet::fillSet(int subSetCardinality) {
    char *tmp = new char[N + 1];
    for (char i = 0; i < N; i++)
        tmp[i] = i + '0';
    for (int i = 0; i < subSetCardinality; i++) {
        int index = rand() % (N - i);

        char c = tmp[i];
        tmp[i] = tmp[i + index];
        tmp[i + index] = c;
    }

    set = 0;
    for (int i = 0; i < subSetCardinality; i++)
        setBit(tmp[i] - '0');

    delete[] tmp;
}

void ComputerWordSet::setBit(int i) {
    set |= 1 << i;
}

ComputerWordSet::ComputerWordSet(int power) {
    this->power = power;
    fillSet(power);
}

ComputerWordSet::ComputerWordSet(const ComputerWordSet &B) {
    power = B.power;
    set = B.set;
}

std::ostream &operator<<(std::ostream &os, const ComputerWordSet &set) {
    os << "computerWordSet: ";
    if (set.power == 0)
        os << " empty";
    for (int i = 0; i < set.N; i++)
        if ((set.set >> i) & 1)
            os << (char) (i + '0') << " ";

    os << std::endl;
    return os;
}

ComputerWordSet &ComputerWordSet::operator|(const ComputerWordSet &B) const {
    ComputerWordSet *res = new ComputerWordSet;
    res->set = this->set | B.set;
    res->power = countBits(res->set);
    return *res;
}

ComputerWordSet &ComputerWordSet::operator&(const ComputerWordSet &B) const {
    ComputerWordSet *res = new ComputerWordSet;
    res->set = this->set & B.set;
    res->power = countBits(res->set);
    return *res;
}

ComputerWordSet &ComputerWordSet::operator=(const ComputerWordSet &B) {
    power = B.power;
    set = B.set;
}

int ComputerWordSet::countBits(int i) const {
    int res = 0;
    for (int j = 0; j < N; j++)
        if ((1 >> j) & 1)
            res++;
    return res;
}
