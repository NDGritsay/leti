#ifndef PROGRAMMING_3_2_LISTSET_H
#define PROGRAMMING_3_2_LISTSET_H


#include <ostream>

class ListElemSet {
public:
    char c;
    ListElemSet *pNext = nullptr;
};

class ListSet {
private:

    ListElemSet *head;

    static const int N = 10;

    bool contains(char elem) const;

    void fillSet(int n);

    void deleteListElemSet(ListElemSet *set);

    void addElem(const char c);

    void swapElem(int i1, int i2);

    int power;


public:

    ListSet();

    virtual ~ListSet();

    ListSet(int power);

    ListSet(const ListSet &);

    ListSet(ListSet &&B);

    ListSet &operator=(const ListSet &);

    ListSet &operator=(ListSet &&);

    ListSet &operator|(const ListSet &) const;

    ListSet &operator&(const ListSet &) const;

    friend std::ostream &operator<<(std::ostream &os, const ListSet &set);
};


#endif
