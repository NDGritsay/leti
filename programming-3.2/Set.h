
#ifndef PROGRAMMING_3_1_SET_H
#define PROGRAMMING_3_1_SET_H


#include <ostream>

class Set {
private:
    int power;
    static const int N = 10;
    static const char EOS = 'a';
    char *set;

    bool contains(char a) const;

    void fillSet(int n);

public:
    Set();

    Set(int power);

    Set(const Set &);

    Set(Set &&);

    virtual ~Set();

    friend std::ostream &operator<<(std::ostream &os, const Set &set);

    Set& operator|(const Set &) const;

    Set& operator&(const Set &) const;

    Set& operator=(const Set &);

    Set& operator=(Set &&);
};


#endif //PROGRAMMING_3_1_SET_H
