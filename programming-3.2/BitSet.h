#ifndef PROGRAMMING_3_2_BITSET_H
#define PROGRAMMING_3_2_BITSET_H


#include <ostream>

class BitSet {
private:
    int power;
    static const int N = 10;
    bool *set;

    bool contains(int i) const;

    void fillSet(int n);

    void setBit(int i);

public:
    BitSet();

    BitSet(int power);

    BitSet(const BitSet &);

    BitSet(BitSet &&);

    virtual ~BitSet();

    friend std::ostream &operator<<(std::ostream &os, const BitSet &set);

    BitSet &operator|(const BitSet &) const;

    BitSet &operator&(const BitSet &) const;

    BitSet &operator=(const BitSet &);

    BitSet &operator=(BitSet &&);
};


#endif //PROGRAMMING_3_2_BITSET_H
