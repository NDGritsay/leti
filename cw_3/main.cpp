#include <iostream>
#include <afxres.h>
#include <cstdio>
#include "Graph.h"

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    system("title=Algorithms Course Work. Graphs");

    bool isProgEnd = false;
    do
    {
        system("cls");
        int vertexCt = Graph::inputVertexCount();
        system("cls");
        bool **graphMatrix = Graph::makeGraphMatrix(vertexCt);
        Graph graph = Graph(vertexCt, graphMatrix);
        system("cls");
        std::cout << "Original graph:\n";
        graph.print();

        if(graph.isBipartite())
            std::cout << "\nGraph already is bipartite.\n";
        else
        {
            Graph *bipartiteSubgraph = graph.makeBipartiteSubgraph();
            std::cout << "\n\nbipartite component:\n";
            bipartiteSubgraph->print();
            rewind(stdin);
            std::cout << "\n";
        }
        system("pause");
        system("cls");

        char c;
        do
        {
            rewind(stdin);
            std::cout << "Do you want to continue?(y/n): ";

            if((c = getchar()) != 'y' && c != 'n')
                std::cout << "\aError! Input correct answer.\n";
        } while (c != 'y' && c != 'n');
        isProgEnd = (c == 'n');
    }while(!isProgEnd);

    return 0;
}