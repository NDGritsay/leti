//
// Created by Nikita Gritsay on 10/22/2016.
//

#ifndef CW_3_EARL_H
#define CW_3_EARL_H


class Graph
{
public:
    Graph(int vertexCt, bool** graphMatrix);
	Graph(int vertexCt, int* vertexName, bool** graphMatrix);
	Graph(Graph **subGraphArray, int subGraphCt);
    ~Graph();

    Graph operator=(const Graph &graph);

    bool isBipartite();
	Graph *makeBipartiteSubgraph();
    static bool** makeGraphMatrix(int vertexCt);
    static int inputVertexCount();
    void print();
private:
    int vertexCt;
    int *vertexName;
    bool **graphMatrix;

	int edgesCt();
    Graph *makeComponent(int *vertices, int vertexCt);
	Graph *makeBipartiteSubcomponent();
    bool bipartiteFillVertex(int vertexId, int currentColor, int *vertexColorMap);
	void componentFillVertex(int vertexId, int currentColor, int *vertexColorMap, int *componentVertexCt);
	int componentCt(int ***componentVertexId, int **componentVertexCt);

    const static int COLOR_EMPTY = 0;
    const static int COLOR_1 = 1;
    const static int COLOR_2 = 2;
    const static int VERTEX_CT_MAX = 26;
};


#endif //CW_3_EARL_H
