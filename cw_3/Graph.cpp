//
// Created by Nikita Gritsay on 10/22/2016.
//

#include <iostream>
#include <queue>
#include <cstdio>
#include "Graph.h"

Graph::Graph(int vertexCt, bool **graphMatrix)
{
    this->vertexCt = vertexCt;
    this->graphMatrix = graphMatrix;
    this->vertexName = new int[vertexCt];
    for(int i = 0; i < vertexCt; i++)
        vertexName[i] = i+1;
}

bool Graph::isBipartite()
{
    bool isRes = true;
    int color = COLOR_1;
    int vertexColor[vertexCt];

    for(int i = 0; i < vertexCt; i++)
        vertexColor[i] = COLOR_EMPTY;

    for(int i = 0; i < vertexCt && isRes; i++)
        if(vertexColor[i] == COLOR_EMPTY)
            isRes = bipartiteFillVertex(i, COLOR_1, vertexColor);

    delete[] vertexColor;
    return isRes;
}

bool Graph::bipartiteFillVertex(int vertexId, int color, int *vertexColorMap)
{
    bool isRes = true;

    vertexColorMap[vertexId] = color;
    for(int i = 0; i < vertexCt && isRes; i++)
        if(graphMatrix[vertexId][i] == true)
        {
            if (vertexColorMap[i] == COLOR_EMPTY)
            {
                int nextColor = (color == COLOR_1) ? COLOR_2 : COLOR_1;
                isRes = bipartiteFillVertex(i, nextColor, vertexColorMap);
            }
            else if(vertexColorMap[i] == color)
                isRes = false;
        }
    return isRes;
}

bool**Graph::makeGraphMatrix(int vertexCt)
{
    bool **graphMatrix = new bool*[vertexCt];
    for(int i = 0; i < vertexCt; i++)
        graphMatrix[i] = new bool[vertexCt];

    char c;

    for(int i = 0; i < vertexCt; i++)
        for(int j = 0; j < vertexCt; j++)
            graphMatrix[i][j] = false;

    do
    {
        do
        {
            rewind(stdin);
            std::cout << "Do you want to add new edge?(y/n): ";

            if((c = getchar()) != 'y' && c != 'n')
                std::cout << "\aError! Input correct answer.\n";
        } while (c != 'y' && c != 'n');

        if(c == 'y')
        {
            int vertex1, vertex2;
            bool isInputCorrect;

            isInputCorrect = false;
            do
            {
                std::cout << "Input first vertex id: ";
                rewind(stdin);

                if(scanf("%d", &vertex1) != 1)
                    std::cout << "\aError! Input number.\n";
                else if(vertex1 < 1 || vertex1 > vertexCt)
                    std::cout << "\aError! Wrong number.\n";
                else
                    isInputCorrect = true;
            } while(!isInputCorrect);

            isInputCorrect = false;
            do
            {
                std::cout << "Input second vertex id: ";
                rewind(stdin);

                if(scanf("%d", &vertex2) != 1)
                    std::cout << "\aError! Input number.\n";
                else if(vertex2 < 1 || vertex2 > vertexCt || vertex1 == vertex2)
                    std::cout << "\aError! Wrong number.\n";
                else
                    isInputCorrect = true;
            } while(!isInputCorrect);

            if(graphMatrix[vertex1 - 1][vertex2 - 1] != true)
            {
                graphMatrix[vertex1 - 1][vertex2 - 1] = true;
                graphMatrix[vertex2 - 1][vertex1 - 1] = true;
                std::cout << "Edge added.\n";
            }
            else
                std::cout << "Edge already has added.";
        }
    } while(c != 'n');

    return graphMatrix;
}

void Graph::print()
{
    printf("   ");
    for(int i = 0; i < vertexCt; i++)
        printf("%3d", this->vertexName[i]);

    for(int i = 0; i < vertexCt; i++)
    {
        printf("\n%2d|", this->vertexName[i]);
        for (int j = 0; j < vertexCt; j++)
            printf("%3d", graphMatrix[i][j] ? 1 : 0);
    }

	printf("\nvertex count: %d, edges count: %d", vertexCt, edgesCt());
}

Graph::~Graph()
{
    delete[](graphMatrix);
}

Graph *Graph::makeBipartiteSubcomponent()
{
    Graph *result = nullptr;

	if(this->isBipartite())
		result = this;
	else {
		bool isSubcomponentReady = false;
		int minEdgesLost = this->edgesCt();

		for (int i = 0; i < vertexCt && !isSubcomponentReady; i++) {
			Graph *temp = this;
			int edgesLost = 0;

			int *verticesColor = new int[temp->vertexCt];
			for (int i = 0; i < temp->vertexCt; i++)
				verticesColor[i] = COLOR_EMPTY;

			std::queue<int> verticesQueue;
			verticesQueue.push(i);
			verticesColor[i] = COLOR_1;
			while(!verticesQueue.empty()){
				int currentId = verticesQueue.front();
				verticesQueue.pop();

				for(int i = 0; i < temp->vertexCt; i++)
					if(temp->graphMatrix[currentId][i] == true){
						if(verticesColor[i] == COLOR_EMPTY){
							verticesQueue.push(i);
							verticesColor[i] = (verticesColor[currentId] == COLOR_1)? COLOR_2 : COLOR_1;
						}
						else if(verticesColor[i] == verticesColor[currentId]){
							graphMatrix[i][currentId] = false;
							graphMatrix[currentId][i] = false;
							edgesLost++;
						}
					}
			}

			if(edgesLost < minEdgesLost){
				minEdgesLost = edgesLost;
				result = temp;

				if(edgesLost == 1)
					isSubcomponentReady = true;
			}
            else
                delete(temp);
		}
	}

    return result;
}

Graph *Graph::makeComponent(int *vertices, int vertexCt)
{
    bool **newGraphMatrix = new bool*[vertexCt];
    for(int i = 0; i < vertexCt; i++)
        newGraphMatrix[i] = new bool[vertexCt];

    for(int i = 0; i < vertexCt; i++)
        for(int j = 0; j < vertexCt; j++)
            newGraphMatrix[i][j] = this->graphMatrix[vertices[i]][vertices[j]];

    /*int *vertexName = new int[vertexCt];
    for(int i = 0; i < vertexCt; i++)
        vertexName[i] = this->vertexName[vertices[i]];*/

    Graph *res = new Graph(vertexCt, newGraphMatrix);

    res->vertexName = new int[vertexCt];
    for(int i = 0; i < vertexCt; i++)
        res->vertexName[i] = this->vertexName[vertices[i]];

    return res;
}

int Graph::inputVertexCount()
{
    int vertexCt;
    bool isInputCorrect;

    isInputCorrect = false;
    do
    {
        rewind(stdin);
        printf("Input vertex ct(1-%d): ", VERTEX_CT_MAX);

        if(scanf("%d", &vertexCt) != 1)
            std::cout << "\aError! Input a number.\n";
        else if(vertexCt < 1 || vertexCt > VERTEX_CT_MAX)
            std::cout << "\aError! Wrong number.\n";
        else
            isInputCorrect = true;
    } while(!isInputCorrect);

    return vertexCt;
}

Graph Graph::operator=(const Graph &graph)
{
    Graph res = Graph(graph.vertexCt, nullptr);

    res.graphMatrix = new bool*[graph.vertexCt];
    for(int i = 0; i < graph.vertexCt; i++)
        res.graphMatrix[i] = new bool[graph.vertexCt];

    for (int i = 0; i < graph.vertexCt; i++)
        for (int j = 0; j < graph.vertexCt; j++)
            res.graphMatrix[i][j] = graph.graphMatrix[i][j];

    res.vertexName = new int[res.vertexCt];
    for(int i = 0; i < res.vertexCt; i++)
        res.vertexName[i] = graph.vertexName[i];

    return res;
}

int Graph::edgesCt() {
	int res = 0;

	for(int i = 0; i < vertexCt; i++)
		for(int j = 0; j < i; j++)
			if(graphMatrix[i][j] == true)
				res++;

	return res;
}

int Graph::componentCt(int ***componentVertexId, int **componentVertexCt) {
	int currentColor = 0;
	int vertexColorMap[vertexCt];

	*componentVertexCt = new int[vertexCt];

	for(int i =0; i < vertexCt; i++) {
		vertexColorMap[i] = 0;
		(*componentVertexCt)[i] = 0;
	}

	for(int i = 0; i < vertexCt; i++)
		if(vertexColorMap[i] == 0) {
			currentColor++;
			componentFillVertex(i, currentColor, vertexColorMap, *componentVertexCt + currentColor - 1); //check on adressate error
		}

	*componentVertexId = new int*[currentColor];
	for(int i = 0; i < currentColor; i++)
		(*componentVertexId)[i] = new int[(*componentVertexCt)[i]];

	int *componentVertexIdFill = new int[currentColor];
	for(int i = 0; i < currentColor; i++)
		componentVertexIdFill[i] = 0;

	for(int i = 0; i < vertexCt; i++)
		(*componentVertexId)[vertexColorMap[i] - 1][componentVertexIdFill[vertexColorMap[i] - 1]++] = i;

	return currentColor;
}

void Graph::componentFillVertex(int vertexId, int currentColor, int *vertexColorMap, int *componentVertexCt) {
	vertexColorMap[vertexId] = currentColor;
	(*componentVertexCt)++;
	for(int i = 0; i < vertexCt; i++)
		if(graphMatrix[vertexId][i] == true && vertexColorMap[i] != currentColor)
			componentFillVertex(i, currentColor, vertexColorMap, componentVertexCt);
}

Graph::Graph(Graph **subGraphArray, int subGraphCt) {
	this->vertexCt = 0;
	for(int i = 0; i < subGraphCt; i++){
		this->vertexCt += (*(subGraphArray + i))->vertexCt;
	}

	this->graphMatrix = new bool*[this->vertexCt];
	for(int i = 0; i < this->vertexCt; i++) {
        this->graphMatrix[i] = new bool[this->vertexCt];
        for(int j = 0; j < this->vertexCt; j++)
            this->graphMatrix[i][j] = false;
    }

	this->vertexName = new int[this->vertexCt];

	int vertexIterator = 0;
	for(int i = 0; i < subGraphCt; i++){
		for(int j = 0; j < (*(subGraphArray + i))->vertexCt; j++)
			for(int k = 0; k < (*(subGraphArray + i))->vertexCt; k++){
				this->graphMatrix[vertexIterator + j][vertexIterator + k] =
						(*(subGraphArray + i))->graphMatrix[j][k];
				this->graphMatrix[vertexIterator + k][vertexIterator + j] =
						(*(subGraphArray + i))->graphMatrix[j][k];
			}

		for(int j = 0; j < (*(subGraphArray + i))->vertexCt; j++)
			this->vertexName[vertexIterator + j] = (*(subGraphArray + i))->vertexName[j];

		vertexIterator += (*(subGraphArray + i))->vertexCt;
	}
}


Graph *Graph::makeBipartiteSubgraph(){
	Graph *result;

	if(this->isBipartite())
		result = this;
	else{
		int **componentVertexId, *componentVertexCt, componentCt;
		componentCt = this->componentCt(&componentVertexId, &componentVertexCt);

		Graph **components = new Graph*[componentCt];
		for(int i = 0; i < componentCt; i++)
			components[i] = this->makeComponent(componentVertexId[i], componentVertexCt[i])->makeBipartiteSubcomponent();

		result = new Graph(components, componentCt);
	}

	return result;
}

Graph::Graph(int vertexCt, int *vertexName, bool **graphMatrix) {
    this->graphMatrix = graphMatrix;
    this->vertexCt = vertexCt;
    this->vertexName = vertexName;
}
