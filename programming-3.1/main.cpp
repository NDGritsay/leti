#include <iostream>
#include <Windows.h>
#include <ctime>
#include <chrono>


using namespace std;

struct Set;

int inputValue(int min, int max, char *valueName);

char *inputArraySet(char *name);

char *procArraySet(char *a, char *b, char *c, char *d);

bool arraySetContains(char *a, char x);

void printArraySet(char *set);

Set *arraySetToListSet(char *aSet);

Set * addListSetElement(Set *tmp, Set *x);

bool listSetContains(Set *set, char x);

Set *procListSet(Set *a, Set *b, Set *c, Set *d);

void printListSet(Set *set);

int charToBitPosition(char c);

char bitPositionToChar(int pos);

bool *arraySetToBitArray(char *arraySet);

bool *procBitArray(bool *a, bool *b, bool *c, bool *d);

void printBitArray(bool *bitArray);

int arraySetToComputerWord(char *arraySet);

int procComputerWord(int a, int b, int c, int d);

void printComputerWord(int word);

char *generateArraySet(int subSetCardinality);

void deleteListSet(Set *set);

const int CARDIN = 10;
const int OPERATION_CT = 1000000;
const char EOS = 'a';

//Developed by Nikita Gritsay & Evgeny Dolenko, Nanolution comp(c)
//Compiled with MinGW
int main()
{
    system("title = Algorithms LETI 3.1");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(0));

    long long arraySetTime[10], listSetTime[10], bitArrayTime[10], computerWordTime[10];

    cout << "Welcome to algorithm complexity counter!\n";
    system("pause");

    cout << "\nprocess has started";
    for(int i = 0; i <= CARDIN; i++)
    {
        std::chrono::high_resolution_clock::time_point startTime, finishTime;
        char *arraySet1, *arraySet2, *arraySet3, *arraySet4, *resArraySet;

        //generation
        arraySet1 = generateArraySet(i);
        arraySet2 = generateArraySet(i);
        arraySet3 = generateArraySet(i);
        arraySet4 = generateArraySet(i);


        //arraySet processing
        startTime = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < OPERATION_CT; j++)
        {
            resArraySet = procArraySet(arraySet1, arraySet2, arraySet3, arraySet4);
            delete[] resArraySet;
        }
        finishTime = std::chrono::high_resolution_clock::now();
        arraySetTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime-startTime).count();


        //listSetProcessing
        Set *listSet1, *listSet2, *listSet3, *listSet4, *resListSet;
        listSet1 = arraySetToListSet(arraySet1);
        listSet2 = arraySetToListSet(arraySet2);
        listSet3 = arraySetToListSet(arraySet3);
        listSet4 = arraySetToListSet(arraySet4);

        startTime = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < OPERATION_CT; j++)
        {
            resListSet = procListSet(listSet1, listSet2, listSet3, listSet4);
            deleteListSet(resListSet);
        }
        finishTime = std::chrono::high_resolution_clock::now();
        listSetTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime-startTime).count();


        deleteListSet(listSet1);
        deleteListSet(listSet2);
        deleteListSet(listSet3);
        deleteListSet(listSet4);

        //bitSetProcessing
        bool *bitArray1, *bitArray2, *bitArray3, *bitArray4, *resBitArray;
        bitArray1 = arraySetToBitArray(arraySet1);
        bitArray2 = arraySetToBitArray(arraySet2);
        bitArray3 = arraySetToBitArray(arraySet3);
        bitArray4 = arraySetToBitArray(arraySet4);

        startTime = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < OPERATION_CT; j++)
        {
            resBitArray = procBitArray(bitArray1, bitArray2, bitArray3, bitArray4);
            delete[] resBitArray;
        }
        finishTime = std::chrono::high_resolution_clock::now();
        bitArrayTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime-startTime).count();

        delete[] bitArray1;
        delete[] bitArray2;
        delete[] bitArray3;
        delete[] bitArray4;

        //computerWordProcessing
        int computerWord1, computerWord2, computerWord3, computerWord4, resComputerWord;
        computerWord1 = arraySetToComputerWord(arraySet1);
        computerWord2 = arraySetToComputerWord(arraySet2);
        computerWord3 = arraySetToComputerWord(arraySet3);
        computerWord4 = arraySetToComputerWord(arraySet4);

        startTime = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < OPERATION_CT; j++)
            resComputerWord = procComputerWord(computerWord1, computerWord2, computerWord3, computerWord4);
        finishTime = std::chrono::high_resolution_clock::now();
        computerWordTime[i] = std::chrono::duration_cast<std::chrono::milliseconds>(finishTime-startTime).count();


        delete[] arraySet1;
        delete[] arraySet2;
        delete[] arraySet3;
        delete[] arraySet4;
    }

    cout << "\nprocessing finished correctly!";

    cout << "\narray time: ";
    for(int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(arraySetTime[i]) << " ";

    cout << ".\nlist time: ";
    for(int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(listSetTime[i]) << " ";

    cout << ".\narray of bits time: ";
    for(int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(bitArrayTime[i]) << " ";

    cout << ".\ncomputer words time: ";
    for(int i = 1; i <= CARDIN; i++)
        cout << i << ")" << double(computerWordTime[i]) << " ";
    cout << ".\n";
    system("pause");
}

int inputValue(int min, int max, char *valueName)
{
    bool isInputCorrect = false;
    int value;

    do
    {
        cout << "Input " << valueName << "(" << min << "-" << max << "): ";

        cin >> value;
        if (cin.fail())
        {
            cout << "\aError! You have to input a number.\n";
            cin.clear();
            cin.sync();
        } else if (value < min || value > max)
            cout << "\aError! The value must be from " << min << " to " << max << ".\n";
        else
            isInputCorrect = true;
    } while (!isInputCorrect);

    return value;
}


char *procArraySet(char *a, char *b, char *c, char *d)
{
    char *res = new char[CARDIN + 1];
    char i = 0;

    for (; c[i] != EOS; i++)
        res[i] = c[i];

    for (char j = 0; d[j] != EOS; j++)
        if (!arraySetContains(c, d[j]))
            res[i++] = d[j];

    res[i] = EOS;
    for (char j = 0; a[j] != EOS; j++)
        if (!arraySetContains(res, a[j]) && arraySetContains(b, a[j]))
        {
            res[i++] = a[j];
            res[i] = EOS;
        }

    return res;
}


bool arraySetContains(char *a, char x)
{
    for (char i = 0; a[i] != EOS; i++)
        if (a[i] == x)
            return true;
    return false;
}


char *inputArraySet(char *name)
{
    char *res = new char[CARDIN + 1], i = 0;
    int c;
    bool isInputCorrect;

    do
    {
        isInputCorrect = true;

        cout << "Input set " << name << " : ";

        while (((c = cin.get()) != '\n') && isInputCorrect)
        {
            if (c < '0' || c > '9')
            {
                cout << "\aError. Unexpectable symbols.\n";
                isInputCorrect = false;
            } else
                for (char j = 0; isInputCorrect && j < i; j++)
                    if (res[j] == c)
                    {
                        cout << "\aError. Repeatable elements.\n";
                        isInputCorrect = false;
                    }
            if (isInputCorrect)
                res[i++] = c;
        }
    } while (!isInputCorrect);
    if (isInputCorrect)
        res[i] = EOS;

    return res;
}


void printArraySet(char *set)
{
    if (*set != EOS)
        for (char *c = set; *c != EOS; c++)
            cout << *c;
    else
        cout << "empty set";
}


struct Set
{
    char element = EOS;
    Set *next = nullptr;
};


Set *arraySetToListSet(char *aSet)
{
    if (*aSet == EOS)
        return nullptr;
    else
    {
        Set *res = new Set[1], *tmp = res;
        char c;
        res->element = *aSet;
        while ((c = *(++aSet)) != EOS)
        {
            tmp->next = new Set[1];
            tmp = tmp->next;
            tmp->element = c;
        }
        return res;
    }
}


Set *procListSet(Set *a, Set *b, Set *c, Set *d)
{
    Set *res = nullptr, *tmp = c;

    while (tmp != nullptr)
    {
        res = addListSetElement(res, tmp);
        tmp = tmp->next;
    }

    tmp = d;
    while (tmp != nullptr)
    {
        if (!listSetContains(res, tmp->element))
            res = addListSetElement(res, tmp);
        tmp = tmp->next;
    }

    tmp = a;
    while (tmp != nullptr)
    {
        if (listSetContains(b, tmp->element) && !listSetContains(res, tmp->element))
            res = addListSetElement(res, tmp);
        tmp = tmp->next;
    }

    return res;
}


bool listSetContains(Set *set, char x)
{
    while (set != nullptr)
    {
        if (set->element == x)
            return true;
        set = set->next;
    }

    return false;
}


Set * addListSetElement(Set *head, Set *x)
{
    if (head == nullptr)
    {
        head = new Set[1];
        head->element = x->element;
    } else
    {
        Set *tmp = head;
        while (tmp->next != nullptr)
            tmp = tmp->next;
        tmp->next = new Set[1];
        tmp->next->element = x->element;
    }

    return head;
}


void printListSet(Set *set)
{
    if (set == nullptr)
        cout << "empty set";
    else
        while (set != nullptr)
        {
            cout << set->element;
            set = set->next;
        }
}


int charToBitPosition(char c)
{
    return c - '0';
}


char bitPositionToChar(int pos)
{
    return pos + '0';
}


bool *arraySetToBitArray(char *arraySet)
{
    bool *res = new bool[CARDIN];

    for(int i = 0; i < CARDIN; i++)
        res[i] = false;

    while(*arraySet != EOS)
    {
        res[charToBitPosition(*arraySet)] = true;
        arraySet++;
    }

    return res;
}


bool *procBitArray(bool *a, bool *b, bool *c, bool *d)
{
    bool *res = new bool[CARDIN];

    for(int i = 0; i < CARDIN; i++)
        res[i] = (a[i] && b[i]) || c[i] || d[i];

    return res;
}


void printBitArray(bool *bitArray)
{
    bool isEmpty = true;
    for(int i = 0; i < CARDIN; i++)
        if(bitArray[i] == true)
        {
            isEmpty = false;
            cout << bitPositionToChar(i);
        }
    if(isEmpty)
        cout << "empty set";
}


int arraySetToComputerWord(char *arraySet)
{
    int res = 0;

    while(*arraySet != EOS)
    {
        res = res | (1 << charToBitPosition(*arraySet));
        arraySet++;
    }

    return res;
}


int procComputerWord(int a, int b, int c, int d)
{
    return (a & b) | c | d;
}


void printComputerWord(int word)
{
    if(word != 0)
    {
        for (int i = 0; i < CARDIN; i++)
            if ((word >> i) & 1)
                cout << bitPositionToChar(i);
    } else
        cout << "empty set";
}


char *generateArraySet(int subSetCardinality)
{
    char *res = new char[CARDIN + 1];

    for(int i = 0; i < CARDIN; i++)
        res[i] = i + '0';

    for(int i = 0; i < subSetCardinality; i++)
    {
        int index = rand() % (CARDIN - i);

        char c = res[i];
        res[i] = res[i + index];
        res[i + index] = c;
    }

    res[subSetCardinality] = EOS;

    return res;
}


void deleteListSet(Set *set)
{
    while(set != nullptr)
    {
        Set *tmp = set;
        set = set->next;
        delete[] tmp;
    }
}