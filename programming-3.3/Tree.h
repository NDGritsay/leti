//
// Created by nikit on 9/24/2016.
//

#ifndef PROGRAMMING_3_3_TREE_H
#define PROGRAMMING_3_3_TREE_H

#include "Node.h"

class Tree {
private:
	void freeNode(Node *node);
	int nodesCt;
	int depth;
	void mark(Node *node);
	Node *generate(int maxDepth, int currentDepth);
    void tablePrint(Node *node, int depth, int pos, int consoleWidth);
	void procDepth(Node *node, int depth, bool writePath);
public:
	Node *root;

	Tree (): root(nullptr), nodesCt(0){};
	~Tree() {freeNode(root);};

	int getDepth(){return depth;};

	void generate(int maxDepth) {root = generate(maxDepth, 0);};
	void mark() {mark(root);};
    void tablePrint();
	void procDepth(bool writePath) {depth = 0; procDepth(root, 0, writePath);};
};


#endif //PROGRAMMING_3_3_TREE_H
