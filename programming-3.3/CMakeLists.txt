cmake_minimum_required(VERSION 3.6)
project(programming_3_3)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Node.h Tree.cpp Tree.h)
add_executable(programming_3_3 ${SOURCE_FILES})