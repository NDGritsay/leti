//
// Created by nikit on 9/24/2016.
//

#ifndef PROGRAMMING_3_3_NODE_H
#define PROGRAMMING_3_3_NODE_H

#include "Tree.h"

class Node {
private:
	int id;
	Node *left;
	Node *right;
public:
	Node(): left(nullptr), right(nullptr){};
	~Node(){};
	friend class Tree;
};


#endif //PROGRAMMING_3_3_NODE_H
