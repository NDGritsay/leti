#include <iostream>
#include <Windows.h>
#include <time.h>
#include "Tree.h"

using std::cout;
using std::endl;

void fullscr();

int main()
{
	srand(time(NULL));
	fullscr();

    Tree tree = Tree();
    tree.generate(8);
    tree.mark();
    tree.tablePrint();
	std::cout << "Path: ";
	tree.procDepth(true);
	std::cout << ".\nDepth of tree = " << tree.getDepth();
    getchar();
    return 0;
}


void fullscr(){
	keybd_event(VK_MENU, 0x38, 0, 0);
	keybd_event(VK_RETURN, 0x1c, 0, 0);
	keybd_event(VK_RETURN, 0x1c, KEYEVENTF_KEYUP, 0);
	keybd_event(VK_MENU, 0x38, KEYEVENTF_KEYUP, 0);
}