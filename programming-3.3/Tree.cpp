//
// Created by nikit on 9/24/2016.
//

#include <cstdlib>
#include <afxres.h>
#include <iostream>
#include "conio.h"
#include "cmath"
#include "Tree.h"


void Tree::freeNode(Node *node)
{
	if(node != nullptr)
	{
		Tree::freeNode(node->left);
		Tree::freeNode(node->right);
		delete node;
	}
}


void Tree::mark(Node *node)
{
	if(node != nullptr)
	{
		Tree::mark(node->left);
		Tree::mark(node->right);
		node->id = ++this->nodesCt;
	}
}


Node *Tree::generate(int maxDepth, int currentDepth)
{
	Node *res = nullptr;

	bool isCreateNode = currentDepth < (rand()%maxDepth+1);
	if(isCreateNode)
	{
		res = new Node;
		res->left = Tree::generate(maxDepth, currentDepth + 1);
		res->right = Tree::generate(maxDepth, currentDepth + 1);
	}

	return res;
}

void Tree::tablePrint(Node *node, int depth, int pos, int consoleWidth)
{
    if(node != nullptr)
    {
        COORD cursorPos;
        cursorPos.X = (short)((consoleWidth/pow(2, depth - 1) * (pos - 1)) + (consoleWidth/pow(2, depth)));
        cursorPos.Y = (short)(depth * 3);
        SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ), cursorPos);
        std::cout << node->id;

        Tree::tablePrint(node->left, depth + 1, pos * 2 - 1, consoleWidth);
        Tree::tablePrint(node->right, depth + 1, pos * 2, consoleWidth);
    }
}

void Tree::tablePrint()
{
    system("cls");

    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);

    Tree::tablePrint(this->root, 1, 1, csbi.srWindow.Right - csbi.srWindow.Left);

	COORD cursorPos;
	procDepth(false);
	cursorPos.X = 0;
	cursorPos.Y = (short)(this->depth * 3 + 3);
	SetConsoleCursorPosition(GetStdHandle( STD_OUTPUT_HANDLE ), cursorPos);
}

void Tree::procDepth(Node *node, int depth, bool writePath)
{
	if(node == nullptr)
	{
		if(depth > this->depth)
			this->depth = depth;
	}
	else
	{
		if (writePath)
			std::cout << " " << node->id;

		procDepth(node->left, depth + 1, writePath);
		procDepth(node->right, depth + 1, writePath);
	}
}